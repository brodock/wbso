package main

import (
	"log"
	"os"

	"github.com/urfave/cli"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/brodock/wbso/contributions"
)

func main() {
	if err := newApp().Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func newApp() *cli.App {
	app := cli.NewApp()
	app.Name = "wbso"
	app.Usage = "Export GitLab contributions for WBSO audits"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "token",
			Value: "",
			Usage: "GitLab token to authenticate the requests and have a access to more contributions",
		},
		cli.StringFlag{
			Name:  "output",
			Value: "out.csv",
			Usage: "Path to file to write the CSV to",
		},
	}

	app.Action = func(ctx *cli.Context) error {
		gl := gitlab.NewClient(nil, ctx.String("token"))
		return contributions.ExportToCSV(gl, ctx.String("output"))
	}

	return app
}
