package contributions

import (
	"fmt"
	"time"

	"github.com/xanzy/go-gitlab"
)

var client *gitlab.Client
var projects map[int]gitlab.Project

func currentUsername() (string, error) {
	user, _, err := client.Users.CurrentUser()
	if err != nil {
		return "", err
	}

	return user.Username, nil
}

func contributionsFromPeriod(username string, fromDate, untilDate time.Time) ([]*contribution, error) {
	after := gitlab.ISOTime(fromDate)
	before := gitlab.ISOTime(untilDate)

	glContributions, err := requestEvents(username, &before, &after)
	if err != nil {
		return nil, err
	}

	for _, contrib := range glContributions {
		retrieveProjectInfo(contrib)
	}

	return glContributions, nil
}

func retrieveProjectInfo(contrib *contribution) {
	if projects == nil {
		projects = make(map[int]gitlab.Project)
	}

	if project, exists := projects[contrib.ProjectID]; exists {
		contrib.ProjectNamespace = project.PathWithNamespace
	} else {
		fmt.Printf("Retrieving project information for ID: %d ", contrib.ProjectID)

		project, _, err := client.Projects.GetProject(contrib.ProjectID, nil, nil)
		if err != nil {
			fmt.Println("Failed!")
			fmt.Println(err)
			return
		}
		fmt.Println("(OK)")

		projects[contrib.ProjectID] = *project
		contrib.ProjectNamespace = project.PathWithNamespace
	}
}

func requestEvents(username string, before, after *gitlab.ISOTime) ([]*contribution, error) {
	fmt.Print("Retrieving contributions")

	opt := &gitlab.ListContributionEventsOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
		After:  after,
		Before: before,
	}

	var contributions []*contribution

	for {
		c, resp, err := client.Users.ListUserContributionEvents(username, opt, nil)
		if err != nil {
			return nil, err
		}

		for _, contrib := range c {
			contributions = append(contributions, fromGitLabContributionEvent(contrib))
		}

		// Exit the loop when we've seen all pages.
		if resp.CurrentPage >= resp.TotalPages {
			fmt.Println("Done!")

			break
		}

		fmt.Print(".")

		// Update the page number to get the next page.
		opt.Page = resp.NextPage
	}

	return contributions, nil
}
