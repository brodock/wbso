package contributions

import (
	"testing"
	"time"

	"github.com/xanzy/go-gitlab"
)

func TestHeaders(t *testing.T) {
	now := time.Now()

	mockContrib := &gitlab.ContributionEvent{
		Title:      "foo",
		ProjectID:  1,
		ActionName: "push",
		TargetID:   1,
		TargetIID:  11,
		TargetType: "Issue",
		CreatedAt:  &now,
	}

	if len(fromGitLab(mockContrib)) != len(csvHeaders) {
		t.Fail()
	}
}
