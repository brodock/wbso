package contributions

import (
	"encoding/csv"
	"os"
	"reflect"
	"strconv"
	"time"

	"github.com/xanzy/go-gitlab"
)

type contribution struct {
	Host             string
	ProjectID        int
	ProjectNamespace string
	ActionName       string
	TargetID         int
	TargetIID        int
	TargetType       string
	AuthorID         int
	TargetTitle      string
	CreatedAtUnix    int64
	AuthorUsername   string
}

func ExportToCSV(gl *gitlab.Client, csvPath string) error {
	file, err := os.OpenFile(csvPath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		return err
	}
	defer file.Close()

	client = gl
	sink := csv.NewWriter(file)
	defer sink.Flush()

	if err := sink.Write(csvHeaders); err != nil {
		return err
	}

	username, err := currentUsername()
	if err != nil {
		return err
	}

	fromDate := time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC)
	untilDate := time.Date(2019, time.December, 31, 23, 59, 59, 0, time.UTC)

	records, err := contributionsFromPeriod(username, fromDate, untilDate)
	if err != nil {
		return err
	}

	for _, record := range records {
		if err := sink.Write(toCsvLine(record)); err != nil {
			return err
		}
	}

	return nil
}

func toCsvLine(c *contribution) []string {
	value := reflect.ValueOf(*c)
	var output []string
	for i := 0; i < value.NumField(); i++ {
		fi := value.Field(i).Interface()
		switch fi.(type) {
		case int:
			output = append(output, strconv.Itoa(fi.(int)))
		case int64:
			output = append(output, strconv.Itoa(int(fi.(int64))))
		case string:
			output = append(output, value.Field(i).String())
		}
	}

	return output
}

func fromGitLabContributionEvent(c *gitlab.ContributionEvent) *contribution {
	return &contribution{
		Host:           "gitlab.com",
		ProjectID:      c.ProjectID,
		ActionName:     c.ActionName,
		TargetID:       c.TargetID,
		TargetIID:      c.TargetIID,
		TargetType:     c.TargetType,
		AuthorID:       c.AuthorID,
		TargetTitle:    c.TargetTitle,
		CreatedAtUnix:  c.CreatedAt.Unix(),
		AuthorUsername: c.AuthorUsername,
	}
}

var csvHeaders = []string{
	"host",
	"project_id",
	"project_namespace",
	"action_name",
	"target_id",
	"target_iid",
	"target_type",
	"author_id",
	"target_title",
	"created_at_unix",
	"author_username",
}
