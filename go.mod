module gitlab.com/brodock/wbso

go 1.13

require (
	github.com/urfave/cli v1.22.1
	github.com/xanzy/go-gitlab v0.20.1
)
